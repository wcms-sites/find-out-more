core = 7.x
api = 2

; library: d3.chart
libraries[d3.chart][download][type] = "git"
libraries[d3.chart][download][url] = "gitlab@git.uwaterloo.ca:mur-dev/d3.chart.git"
libraries[d3.chart][download][tag] = "7.x-1.0"

; library: d3.chart.sankey
libraries[d3.chart.sankey][download][type] = "git"
libraries[d3.chart.sankey][download][url] = "gitlab@git.uwaterloo.ca:mur-dev/d3.chart.sankey.git"
libraries[d3.chart.sankey][download][tag] = "7.x-1.0"

; library: d3-plugins-sankey
libraries[d3-plugins-sankey][download][type] = "git"
libraries[d3-plugins-sankey][download][url] = "gitlab@git.uwaterloo.ca:mur-dev/d3-plugins-sankey.git"
libraries[d3-plugins-sankey][download][tag] = "7.x-1.0"

; composer_manager
projects[composer_manager][type] = "module"
projects[composer_manager][download][type] = "git"
projects[composer_manager][download][url] = "https://git.uwaterloo.ca/drupal-org/composer_manager.git"
projects[composer_manager][download][tag] = "7.x-1.8"
projects[composer_manager][subdir] = ""

; d3
projects[d3][type] = "module"
projects[d3][download][type] = "git"
projects[d3][download][url] = "https://git.uwaterloo.ca/drupal-org/d3.git"
projects[d3][download][tag] = "7.x-1.0-alpha2"
projects[d3][subdir] = ""

; d3_sankey
projects[d3_sankey][type] = "module"
projects[d3_sankey][download][type] = "git"
projects[d3_sankey][download][url] = "https://git.uwaterloo.ca/drupal-org/d3_sankey.git"
projects[d3_sankey][download][branch] = "7.x-1.x"
projects[d3_sankey][download][revision] = "2157c0c"
projects[d3_sankey][subdir] = ""

; sankey_diagram_content_type
projects[sankey_diagram_content_type][type] = "module"
projects[sankey_diagram_content_type][download][type] = "git"
projects[sankey_diagram_content_type][download][url] = "gitlab@git.uwaterloo.ca:mur-dev/sankey_diagram_content_type.git"
projects[sankey_diagram_content_type][download][tag] = "7.x-1.0.1"
projects[sankey_diagram_content_type][subdir] = ""

; uw_career_paths
projects[uw_career_paths][type] = "module"
projects[uw_career_paths][download][type] = "git"
projects[uw_career_paths][download][url] = "gitlab@git.uwaterloo.ca:mur-dev/uw_career_paths.git"
projects[uw_career_paths][download][tag] = "7.x-1.0"
projects[uw_career_paths][subdir] = ""

; xautoload
projects[xautoload][type] = "module"
projects[xautoload][download][type] = "git"
projects[xautoload][download][url] = "https://git.uwaterloo.ca/drupal-org/xautoload.git"
projects[xautoload][download][tag] = "7.x-5.7"
projects[xautoload][subdir] = ""

; uw_admissions
projects[uw_admissions][type] = "module"
projects[uw_admissions][download][type] = "git"
projects[uw_admissions][download][url] = "gitlab@git.uwaterloo.ca:mur-dev/uw_admissions.git"
projects[uw_admissions][download][tag] = "7.x-2.2.1"
projects[uw_admissions][subdir] = ""

; uw_campus_tour_registration
projects[uw_campus_tour_registration][type] = "module"
projects[uw_campus_tour_registration][download][type] = "git"
projects[uw_campus_tour_registration][download][url] = "gitlab@git.uwaterloo.ca:mur-dev/uw_campus_tour_registration.git"
projects[uw_campus_tour_registration][download][tag] = "7.x-3.8"
projects[uw_campus_tour_registration][subdir] = ""

; uw_crm_authorization
projects[uw_crm_authorization][type] = "module"
projects[uw_crm_authorization][download][type] = "git"
projects[uw_crm_authorization][download][url] = "gitlab@git.uwaterloo.ca:mur-dev/uw_crm_authorization.git"
projects[uw_crm_authorization][download][tag] = "7.x-1.0"
projects[uw_crm_authorization][subdir] = ""

; uw_highschool_visits
projects[uw_highschool_visits][type] = "module"
projects[uw_highschool_visits][download][type] = "git"
projects[uw_highschool_visits][download][url] = "gitlab@git.uwaterloo.ca:mur-dev/uw_highschool_visits.git"
projects[uw_highschool_visits][download][tag] = "7.x-2.0.2"
projects[uw_highschool_visits][subdir] = ""

; uw_mur_newsletter
projects[uw_mur_newsletter][type] = "module"
projects[uw_mur_newsletter][download][type] = "git"
projects[uw_mur_newsletter][download][url] = "gitlab@git.uwaterloo.ca:mur-dev/uw_mur_newsletter.git"
projects[uw_mur_newsletter][download][tag] = "7.x-2.6"
projects[uw_mur_newsletter][subdir] = ""

; uw_request_brochure
projects[uw_request_brochure][type] = "module"
projects[uw_request_brochure][download][type] = "git"
projects[uw_request_brochure][download][url] = "gitlab@git.uwaterloo.ca:mur-dev/uw_request_brochure.git"
projects[uw_request_brochure][download][tag] = "7.x-1.8"
projects[uw_request_brochure][subdir] = ""

; uw_studentbudget_calculator
projects[uw_studentbudget_calculator][type] = "module"
projects[uw_studentbudget_calculator][download][type] = "git"
projects[uw_studentbudget_calculator][download][url] = "gitlab@git.uwaterloo.ca:mur-dev/uw_studentbudget_calculator.git"
projects[uw_studentbudget_calculator][download][tag] = "7.x-2.5.2"
projects[uw_studentbudget_calculator][subdir] = ""

; uw_admissions_programs
projects[uw_admissions_programs][type] = "module"
projects[uw_admissions_programs][download][type] = "git"
projects[uw_admissions_programs][download][url] = "gitlab@git.uwaterloo.ca:mur-dev/uw_admissions_programs.git"
projects[uw_admissions_programs][download][tag] = "7.x-1.3.1"
projects[uw_admissions_programs][subdir] = ""

;  uw_mur_event_registration
projects[uw_mur_event_registration][type] = "module"
projects[uw_mur_event_registration][download][type] = "git"
projects[uw_mur_event_registration][download][url] = "gitlab@git.uwaterloo.ca:mur-dev/uw_mur_event_registration.git"
projects[uw_mur_event_registration][download][tag] = "7.x-1.1"
projects[uw_mur_event_registration][subdir] = ""

;  uw_ft_event_registration
projects[uw_ft_event_registration][type] = "module"
projects[uw_ft_event_registration][download][type] = "git"
projects[uw_ft_event_registration][download][url] = "gitlab@git.uwaterloo.ca:mur-dev/uw_ft_event_registration.git"
projects[uw_ft_event_registration][download][tag] = "7.x-1.2"
projects[uw_ft_event_registration][subdir] = ""

;  uw_ct_event_registration
projects[uw_ct_event_registration][type] = "module"
projects[uw_ct_event_registration][download][type] = "git"
projects[uw_ct_event_registration][download][url] = "gitlab@git.uwaterloo.ca:mur-dev/uw_ct_event_registration.git"
projects[uw_ct_event_registration][download][tag] = "7.x-1.0.2"
projects[uw_ct_event_registration][subdir] = ""

;  uw_ct_event_registration_features
projects[uw_ct_event_registration_features][type] = "module"
projects[uw_ct_event_registration_features][download][type] = "git"
projects[uw_ct_event_registration_features][download][url] = "gitlab@git.uwaterloo.ca:mur-dev/uw_ct_event_registration_features.git"
projects[uw_ct_event_registration_features][download][tag] = "7.x-1.0"
projects[uw_ct_event_registration_features][subdir] = ""
